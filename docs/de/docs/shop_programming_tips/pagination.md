# Pagination

Mit der *Pagination* können Sie große Listen von Items (Tabelleneinträge, News-Beiträge, Artikel-Reviews) auf mehrere
Seiten aufteilen, durch die der Betrachter blättern kann. Dabei können ihm Einstellungen zur Seitengröße und zur
Sortierung angeboten werden. Alle festgelegten Einstellungen werden in der Admin-*SESSION* festgehalten.

Die *Pagination* kann auf zwei verschiedene Arten verwendet werden:

* Sie übergeben der *Pagination* ein fertiges Array als Eingabe. Dieses wird sortiert und der momentan anzuzeigende
  Bereich wird ausgeschnitten.
* Sie übergeben der *Pagination* die Gesamtanzahl der Einträge als Eingabe. Das *Paginations*-Objekt liefert dann
  lediglich anhand der gewählten Optionen eine SQL `LIMIT`- und eine `ORDER BY`-Klausel, die Sie in Ihre eigene
  SQL-Abfrage einbauen können.

## Dateien der *Pagination*

| Datei | Funktion |
| - | - |
| `includes/src/Pagination/Pagination.php` | Paginations-Klasse |
| `admin/templates/bootstrap/tpl_inc/pagination.tpl` | Template-Datei für das Backend |
| `templates/NOVA/snippets/pagination.tpl` | Template-Datei für das Frontend (NOVA-Template) |

## Quick-Start

Erzeugen Sie eine Instanz der Pagination. Weisen Sie der neuen Pagination einen ID-String zu, mit dem diese Pagination
und ihre in der *SESSION* gespeicherten Einstellungen von anderen Instanzen unterschieden werden können.

    $oPaginationStandard = new Pagination('standard');

Übergeben Sie der *Pagination* ein Array aller Items, durch die geblättert werden soll: |br|
(Für Daten-Listen, die sich rein aus Datenbankabfragen erzeugen lassen, siehe:
[Eigene SQL-Abfrage](#eigene-sql-abfrage).)

    $oPaginationStandard = (new Pagination('standard'))
        ->setItemArray($oKuponStandard_arr)
        ->setSortByOptions([
            ['cName', 'Name'],
            ['cCode', 'Code'],
            ['nVerwendungenBisher', 'Verwendungen'],
            ['dLastUse', 'Zuletzt verwendet']
        ]);

Wie hier zu sehen ist, sind alle Methoden der Pagination *chainable*. |br|
Zum Schluss stellen Sie die Pagination mit ``assemble()`` fertig:

``` hl_lines="9"
$oPaginationStandard = (new Pagination('standard'))
    ->setItemArray($oKuponStandard_arr)
    ->setSortByOptions([
        ['cName', 'Name'],
        ['cCode', 'Code'],
        ['nVerwendungenBisher', 'Verwendungen'],
        ['dLastUse', 'Zuletzt verwendet']
    ])
    ->assemble();
```

!!! important
    Danach sollten keine *Setter* mehr aufgerufen werden!

Übergeben Sie nun das Paginations-Objekt an Smarty.

    $smarty->assign('oPaginationStandard', $oPaginationStandard);

Die Einträge der momentan gewählten Blätter-Seite erhalten Sie durch `$oPaginationStandard->getPageItems()`.
Durch diese Liste können Sie dann entsprechend iterieren und die darin enthaltenen Elemente im Frontend ausgeben.

    {foreach $oPaginationStandard->getPageItems() as $oKupon}
        ...
    {/foreach}

## Einbindung der Templates

Die Templates enthalten die Seitennavigation und die Kontrollelemente zum Sortieren und Einstellen der Seitengröße.
Es gibt zwei getrennte Templates für das Backend und für das Frontend.

### Backend

    {include file='tpl_inc/pagination.tpl'
        oPagination=$oPagination
        cParam_arr=['tab'=>$tab]
        cAnchor=$tab}

#### Parameter

| Parameter | Verwendung |
| - | - |
| `oPagination` | das Paginations-Objekt |
| `cParam_arr` (optional) | assoziatives Array von GET-Parametern, welche von der Pagination beim Seitenblättern oder Ändern von Optionen mit durchgereicht werden sollen |
| `cAnchor` (optional) | ein zusätzlicher Ziel-Anker, der mit an die URL angehängt wird (Form: ``#foobar``) |

### Frontend

    {include file='snippets/pagination.tpl'
        oPagination=$oPagination
        cParam_arr=['tab'=>$tab]
        cThisUrl='/target/path'
        cParam_arr=['key1' => 'val1', 'key2' => 'val2', ...]
        parts=['pagi', 'label']}

#### Parameter

<table markdown="1">
<tr markdown="1"><th> Parameter </th><th> Verwendung </th></tr>
<tr markdown="1"><td> `oPagination` </td><td> das Pagination-Objekt </td></tr>
<tr markdown="1"><td> `cParam_arr` (optional) </td><td> siehe oben (Backend) </td></tr>
<tr markdown="1"><td> `cThisUrl` (optional) </td><td> eigener Pfad der einbindenden Seite </td></tr>
<tr markdown="1"><td> `parts` (optional) </td><td>

Mit diesem Parameter kann die Anzeige auf einzelne Komponenten des Templates eingeschränkt werden. Übergeben Sie
hier eine Liste von Komponenten-Bezeichnern:

* `label` Label für die Anzahl der Einträge
* `pagi` Seitennavigation
* `count` Selectbox für Einträge pro Seite
* `sort` Selectbox für die Sortierung

</td></tr>
</table>

## Methoden des Paginations-Objekts

<table markdown="1">
<tr markdown="1"><th> Methode </th><th> Funktion </th></tr>
<tr markdown="1"><td> `setRange($nRange)` </td><td>

Da bei sehr großen Listen auch große Seitenzahlen entstehen können, die die Navigation zu lang werden lassen, werden
Auslassungspunkte (`...`) eingefügt. Auf der linken und rechten Seite vom gerade aktiven Seitenlink werden dann
jeweils maximal `$nRange` benachbarte Seitenlinks angezeigt.

</td></tr>
<tr markdown="1"><td> `setItemsPerPageOptions($nItemsPerPageOption_arr)` </td><td>

Legt die Auswahloptionen für "Einträge pro Seite" fest. Diese werden in einer Selectbox zur Auswahl angeboten.

**Beispiel:** `[5, 10, 20, 50]`

</td></tr>
<tr markdown="1"><td> `setSortByOptions($cSortByOption_arr)` </td><td>

Legt die Auswahloptionen für die Sortierung fest. Jede Auswahloption ist ein Paar aus der Tabellenspalte (dem
*Property*, nach dem sortiert wird) und einer zugehörigen Beschriftung. Diese werden in einer Selectbox jeweils für
aufsteigende und absteigende Reihenfolge zur Auswahl angeboten.

**Beispiel:**

    [
        ['cName', 'Name'],
        ['cCode', 'Code'],
        ['nVerwendungenBisher', 'Verwendungen'],
        ['dLastUse', 'Zuletzt verwendet']
    ]

</td></tr>
<tr markdown="1"><td> `setItemArray($oItem_arr)` </td><td>
Legt das gesamte Array aller Items fest (erste Verwendungsmethode)</td></tr>
<tr markdown="1"><td> `setItemCount($nItemCount)` </td><td>
Legt die gesamte Anzahl der Items fest (erste Verwendungsmethode)</td></tr>
<tr markdown="1"><td> `setDefaultItemsPerPage($n)` </td><td>
Setzt die Standard-Seitengröße (Items pro Seite) </td></tr>
<tr markdown="1"><td> `setItemsPerPage($nItemsPerPage)` </td><td>

Übergeht die gewählte Option für "Einträge pro Seite" und legt diese auf den Wert ``$nItemsPerPage`` fest. Dies ist
nützlich, wenn Sie keine Auswahlmöglichkeiten anbieten möchten, sondern einen festen Wert vorgeben wollen. 

</td></tr>
</table>

.. _label_sql_optimized_pagination:

## Eigene SQL-Abfrage

Oft müssen größere Datenmengen *direkt aus der Datenbank* dargestellt werden. Für diesen Zweck existiert eine weitere
Verwendungsmöglichkeit, bei der dem Paginations-Objekt lediglich die Gesamtanzahl der anzuzeigenden Elemente übergeben
wird (mittels `setItemCount()`).

``` hl_lines="1"
$oPagination->setItemCount(
    Shop::Container()->getDB()->query(
        'SELECT count(*) AS count FROM tkunden',
        ReturnType::SINGLE_OBJECT
    )->count);
```

Das Paginations-Objekt ermittelt nun den Ausschnitt der Datenmenge, die der Benutzer beim Blättern angezeigt bekommt.
Anschließend liest das Paginations-Object nur noch diesen "*Datenbereich*" aus der Datenbank, was die Mengen an Daten,
die übertragen werden müssen, erheblich reduziert.

Nach der Fertigstellung mit `assemble()` können Sie dann die gewünschten SQL-Klauseln für `LIMIT`, und bei Bedarf
auch für `ORDER`, vom Paginations-Objekt abrufen (mittels `getLimitSQL()` und `getOrderSQL()`).

Diese SQL-Klauseln können Sie dann in einer eigenen SQL-Abfrage verwenden:

.. code-block:: php
   :emphasize-lines: 4

``` hl_lines="4"
$pageOfData = Shop::Container()->getDB()->queryPrepared(
    'SELECT * FROM tredirect LIMIT :limitation ORDER BY :sorting',
    [
        'limitation' => $oPagination->getLimitSQL(),
        'sorting'    => $oPagination->getOrderSQL()
    ],
    ReturnType::ARRAY_OF_OBJECTS);
```

Abschließend übergeben Sie dann wieder das Paginations-Objekt an Smarty:

    $smarty->assign('pageOfData', $pageOfData);
