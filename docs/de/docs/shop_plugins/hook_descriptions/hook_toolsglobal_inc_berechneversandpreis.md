# HOOK_TOOLSGLOBAL_INC_BERECHNEVERSANDPREIS (106)

## Triggerpunkt

Nach der Berechnung der Versandkosten

## Parameter

* `float|int` **fPreis**; ab 4.06: **&fPreis** - Preis
* `object` **versandart** - Versandart-Objekt
* `string` **cISO** - ISO-String
* `oArtikel|stdClass` **oZusatzArtikel** - Zusatzartikel
* `JTL\Catalog\Product\Artikel|null` **Artikel** - Artikel-Objekt