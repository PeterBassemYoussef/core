# HOOK_BESTELLVORGANG_INC_UNREGISTRIERTBESTELLEN_PLAUSI (76)

## Triggerpunkt

Plausibilitätsprüfung für die unregistrierte Bestellung, im Bestellvorgang

## Parameter

* `int` **&nReturnValue** - Resultat der Plausibilitätsprüfung
* `array` **&fehlendeAngaben** - Liste der fehlenden Angaben
* `JTL\Customer\Kunde` **&Kunde** - Kundenobjekt
* `array` **cPost_arr** - HTTP-POST-Variablen