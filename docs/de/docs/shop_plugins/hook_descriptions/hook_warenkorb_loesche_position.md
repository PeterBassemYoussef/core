# HOOK_WARENKORB_LOESCHE_POSITION (205)

## Triggerpunkt

Vor dem Löschen einer Position im Warenkorb

## Parameter

* `mixed` **nPos** - der Index der Warenkorbposition
* `mixed` **&position** - die Warenkorbposition selbst